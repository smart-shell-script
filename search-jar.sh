#!/bin/sh
echo Params: "$#" - \("$1"\) \("$2"\) \("$3"\) \("$4"\)
if test "$#" -ne 4
then
	echo "Command takes exactly 4 params - Jar_name_pattern search_location file_name_pattern search_string
Use - for Jar_name_pattern to search for within all jars.
Use - for search_file_name to find with file_name_pattern only and avoid content search"
	exit 1
fi

if test "$1" = "-"
then
	jar_param="*.jar"
else
	jar_param="$1"
fi
if test "$4" = "-"
then
	search_content=1
else
	search_content=0
fi
echo Executing: find "$2" -name "$jar_param"
file_list=`find "$2" -name "$jar_param"`
current_dir=`pwd`
cd /tmp/
mkdir search_file || rw_not=1
if test -n "$rw_not"
then
	echo "/tmp/ is not writeable by current user thus can search file content and exiting"
	exit 1
fi
cd search_file
echo "Search Result:"
for jar_file in $file_list
do
	inner_file_list=`jar tf "$jar_file" | grep "$3"`
	if test "$search_content" = "0"
	then
		if test -n "$inner_file_list"
		then
			jar xf "$jar_file" $inner_file_list
			for inner_file in $inner_file_list
			do
				has_content=`cat $inner_file | grep "$4"`
				if test -n "$has_content"
				then
					echo Found in "$inner_file" @ "$jar_file"
				fi
			done
			rm -rf *
		fi
	else
		test -n "$inner_file_list" && echo "$jar_file"
	fi
done
cd ..
rm -rf ./search_file
cd "$current_dir"
